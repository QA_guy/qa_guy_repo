﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace SeleniumTestingPJ
{
    //SeleniumCodedUITests
    [CodedUITest]
    public class SeleniumCodedUITests
    {
        public SeleniumCodedUITests()
        {
        }

        SeleniumCodedUITestsCodeBehind sctCB = new SeleniumCodedUITestsCodeBehind();
        string _testResultsPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile).ToString() + "//source//repos//qa_guy_repo//Selenium//TestResults/test.txt";

        [TestMethod]
        public void HomePage_Test()
        {            
            sctCB.AssertOnHomePageContent("Tours", "HomePage_Test", _testResultsPath);
        }

        [TestMethod]
        public void Navigation_Test()
        {
            sctCB.Navigate_To_Blog_Page_and_Assert_On_Content("//div[contains(@id, 'header_main')]/div/div/nav/div/ul/li", "Blog", "Cape Peninsula Day Tour", "Navigation_Test", _testResultsPath);
        }

        [TestMethod]
        public void Negative_Navigation_Test()
        {
            sctCB.Navigate_To_Blog_Page_and_Assert_On_Content("//div[contains(@id, 'header_main')]/div/div/nav/div/ul/li", "Blog", "Western Cape Peninsula Day Tour", "Negative_Navigation_Test", _testResultsPath);
        }

        [TestInitialize()]
        public void BaseTestInitialize()
        {
            Driver.Inst.Manage().Window.Maximize();
        }
    }
}
