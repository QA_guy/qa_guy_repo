﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumTestingPJ
{
    public class SeleniumCodedUITestsCodeBehind
    {
        private string attributeType = null;
        WebDriverWait wait = new WebDriverWait(Driver.Inst, TimeSpan.FromMinutes(2));

        public void AssertOnHomePageContent(string assertionText, string testName, string testResultsPath)
        {
            Thread.Sleep(5000);
            Assert.IsTrue(Driver.Inst.PageSource.Contains("Tours"));
            System.IO.StreamWriter sw = new System.IO.StreamWriter(testResultsPath, true);

            try
            {
                Assert.IsTrue(Driver.Inst.PageSource.Contains(assertionText));
                sw.WriteLine(testName + " PASSED " + DateTime.Now.ToString());
            }
            catch
            {
                sw.WriteLine(testName + " FAILED " + DateTime.Now.ToString());
            }

            sw.Flush();
        }

        public void Navigate_To_Blog_Page_and_Assert_On_Content(string xpathQuery, string navigationChoice, string assertionText, string testName, string testResultsPath)
        {
            WaitForSpecific_Xpath_to_Exist("span", HTML_Attribute_Type.text, "Tours");
            IWebElement blog_element = Element_ToClick(xpathQuery, navigationChoice);
            blog_element.Click();
            Thread.Sleep(5000);

            System.IO.StreamWriter sw = new System.IO.StreamWriter(testResultsPath, true);

            try
            {
                Assert.IsTrue(Driver.Inst.PageSource.Contains(assertionText));
                sw.WriteLine(testName + " PASSED " + DateTime.Now.ToString());
            }
            catch
            {
                sw.WriteLine(testName + " FAILED " + DateTime.Now.ToString());
            }

            sw.Flush();

        }

        private IWebElement Element_ToClick(string xpathQuery, string elementText)
        {
            IList<IWebElement> xpathElements = Driver.Inst.FindElements(By.XPath(xpathQuery));
            IWebElement _xpathElement = null;

            foreach (IWebElement xpathElement in xpathElements)
            {
                if (xpathElement.Text.Contains(elementText))
                {
                    _xpathElement = xpathElement;
                    break;
                }
            }

            return _xpathElement;
        }

        private void WaitForSpecific_Xpath_to_Exist(string ElementType, HTML_Attribute_Type html_tag_attribute_type, string attrValue)
        {
            // wait declared on line 16 NOT 100% functional when in used inside the try for an implicit wait - therefore explicit wait added below;
            Thread.Sleep(5000);
            string xpathQuery = Xpath_String_Builder(ElementType, html_tag_attribute_type, attrValue);

            try
            {
                wait.Until(drv => drv.FindElement(By.XPath(xpathQuery)));
            }
            catch
            {

            }
        }

        private string Xpath_String_Builder(string elementType, HTML_Attribute_Type html_tag_attribute_type, string attrValue)
        {
            switch (html_tag_attribute_type)
            {
                case HTML_Attribute_Type.Class:
                    attributeType = "@class";
                    break;

                case HTML_Attribute_Type.id:
                    attributeType = "@id";
                    break;

                case HTML_Attribute_Type.title:
                    attributeType = "@title";
                    break;

                case HTML_Attribute_Type.text:
                    attributeType = "text()";
                    break;

                case HTML_Attribute_Type.name:
                    attributeType = "@name";
                    break;
            }
            return String.Format("//{0}[contains({1},'{2}')]", elementType, attributeType, attrValue);
        }

        public enum HTML_Attribute_Type
        {
            Class,
            title,
            id,
            text,
            name
        }
    }
}