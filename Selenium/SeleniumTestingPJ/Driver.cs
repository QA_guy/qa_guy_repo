﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SeleniumTestingPJ
{
    class Driver
    {
        private static Stack<IWebElement> _frameStack = new Stack<IWebElement>();

        private static IWebDriver _unique_instance;
        public static IWebDriver Inst
        {
            get
            {
                if (_unique_instance == null)
                {
                    _unique_instance = DriverFactory.CreateWebDriver(CurrentBrowser);
                    _unique_instance.Url = System.Configuration.ConfigurationManager.AppSettings["StartUpURL"];
                    ClearFrameStack();
                }

                return _unique_instance;
            }
        }

        public static void ClearFrameStack()
        {
            _frameStack.Clear();
            Driver.Inst.SwitchTo().DefaultContent();
        }

        public static class DriverFactory
        {
            public static IWebDriver CreateWebDriver(BrowserType browserType)
            {
                IWebDriver driver = null;

                switch (browserType)
                {                    
                    case BrowserType.Chrome:                        
                        ChromeOptions crOptions = new ChromeOptions();
                        crOptions.AddUserProfilePreference("download.prompt_for_download", false);
                        crOptions.AddArgument("--enable-npapi");
                        crOptions.AddArgument("--always-authorize-plugins=true");
                        crOptions.AddArgument("--ignore-certificate-errors");
                        crOptions.AddArguments("--allow-running-insecure-content");
                        crOptions.AddArguments("--disable-extensions");
                        driver = new ChromeDriver(crOptions);
                        break;                    
                }

                return driver;
            }
        }

        private static BrowserType? _browserType;
        public static BrowserType CurrentBrowser
        {
            get
            {
                if (_browserType == null)
                {
                    _browserType = (BrowserType)int.Parse(System.Configuration.ConfigurationManager.AppSettings["BrowserType"]);
                }
                return (BrowserType)_browserType;
            }
        }

        public enum BrowserType
        {   
            Chrome = 1
        }
    }
}
