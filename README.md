Requirements:

OS         : Windows 10 hopefully (Condolances to us both if you're using anything older than 5 years)
Browser    : Chrome
Powershell : Version 5.1
IDE		   : Visual Studio 2017 hopefully


1)Clone this repo from Visual Studio using the following url: https://QA_guy@bitbucket.org/QA_guy/qa_guy_repo.git
2)Build solution in debug
3)Download chromedriver from following url and copy the exe from the zipped folder into your "C:\Windows\" folder
4)Copy the code in the "RunQA_guy_tests.ps1" file into a new powershell script and save it
5)Open powershell file created in step 4 as administrator
6)Please read the commented out line at the top of the script - if you're using Visual Studio 2017 then leave script as is
7)Run the powershell script
8)After the run has completed, search for and open "test.txt" on your pc {Should be Found in a "TestResults" directory that is created 1 or 2 directory levels deeper from this location: 
  "C:\Users\YOURMACHINENAME\source\repos\" BUT only after step 7 is completed

Three EXTREMLEY BASIC tests are run - 2 will pass and 1 will fail

